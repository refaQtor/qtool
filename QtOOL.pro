#-------------------------------------------------
#
# Project created by QtCreator 2014-07-01T22:23:17
#
#-------------------------------------------------

QT       += core gui xmlpatterns

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtOOL
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    xmlmodel.cpp

HEADERS  += dialog.h \
    xmlmodel.h

FORMS    += dialog.ui

OTHER_FILES += \
    resource/brief.xml \
    resource/brief.xsd

RESOURCES += \
    resources.qrc
