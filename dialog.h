#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_regex_edit_textChanged(const QString &regex_edit_text);

    void on_xml_validate_clicked();

    void on_xml_xsd_textChanged();

    void on_xml_xml_textChanged();

private:
    Ui::Dialog *ui;
    void xmlMoveCursor(int line, int column);
};

#endif // DIALOG_H
