Qtool is a small application the I use to develop with Qt4 regex and xml modules.  

Qt4 regex had some quirks that made it useful to have a little tool that would show me exactly how a particular regex would work alone, before I used it in my other applications.  Qt5 has a new QRegularExpression that behaves much better, and I may not use this tool further for QRegex testing/development.

I also found it useful to work with Qt4 xml module in isolation while developing other applications. So I put this little tool together.