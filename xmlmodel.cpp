/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "xmlmodel.h"

#include <QFileInfo>

XmlModel::XmlModel(QObject *root_object, const QXmlNamePool &namePool) :
    QSimpleXmlNodeModel(namePool),
    _root_object(new QObject)
{
    QXmlNamePool np = namePool;
    m_names.resize(7);
    m_names[Object]         = QXmlName(np, QLatin1String("object"));
    m_names[Field]          = QXmlName(np, QLatin1String("field"));
}


QUrl XmlModel::documentUri(const QXmlNodeModelIndex &ni) const
{
    Q_UNUSED(ni)
    return QUrl("http://eneryplus.nrel.gov/");
}

QXmlNodeModelIndex::NodeKind XmlModel::kind(const QXmlNodeModelIndex &ni) const
{
    switch (Type(ni.additionalData())) {
        case Object:
        case Field:
            return QXmlNodeModelIndex::Element;
        default:
            return QXmlNodeModelIndex::Attribute;
    }
}

QXmlNodeModelIndex::DocumentOrder XmlModel::compareOrder(const QXmlNodeModelIndex &ni1, const QXmlNodeModelIndex &ni2) const
{
    return QXmlNodeModelIndex::Is;
}

QXmlNodeModelIndex XmlModel::root(const QXmlNodeModelIndex &n) const
{
    Q_UNUSED(n);
    return createIndex(_root_object);
}

QXmlName XmlModel::name(const QXmlNodeModelIndex &ni) const
{
    return m_names.at(ni.additionalData());
}

QVariant XmlModel::typedValue(const QXmlNodeModelIndex &n) const
{
//    const QFileInfo &fi = toFileInfo(node);

    switch (Type(n.additionalData())) {
        case Object:
            return QString("object");
        case Field:
            return QString("field");
//        case AttributeFileName:
//            return fi.fileName();
//        case AttributeFilePath:
//            return fi.filePath();
//        case AttributeSize:
//            return fi.size();
//        case AttributeMIMEType:
//            {
//                /* We don't have any MIME detection code currently, so return
//                 * the most generic one. */
//                return QLatin1String("application/octet-stream");
//            }
//        case AttributeSuffix:
//            return fi.suffix();
    }

    Q_ASSERT_X(false, Q_FUNC_INFO, "This line should never be reached.");
    return QString();
}

QXmlNodeModelIndex XmlModel::nextFromSimpleAxis(SimpleAxis axis, const QXmlNodeModelIndex &origin) const
{
//    const QFileInfo fi(toFileInfo(origin));
//    const Type type = Type(nodeIndex.additionalData());

//    if (type != Field && type != Object) {
//        Q_ASSERT_X(axis == Parent, Q_FUNC_INFO, "not Object or Field object!");
//        return toNodeIndex(fi, Object);
//    }

    switch (axis) {
        case Parent:
        ;
//            return toNodeIndex(QFileInfo(fi.path()), Object);

        case FirstChild:
        {
//            if (type == Field) // A file has no children.
//                return QXmlNodeModelIndex();
//            else {
//                Q_ASSERT(type == Object);
//                Q_ASSERT_X(fi.isDir(), Q_FUNC_INFO, "It isn't really a Object!");
//                const QDir dir(fi.absoluteFilePath());
//                Q_ASSERT(dir.exists());

//                const QFileInfoList children(dir.entryInfoList(QStringList(),
//                                                               m_filterAllowAll,
//                                                               m_sortFlags));
//                if (children.isEmpty())
//                    return QXmlNodeModelIndex();
//                const QFileInfo firstChild(children.first());
//                return toNodeIndex(firstChild);
//            }
        }

        case PreviousSibling:
//            return nextSibling(nodeIndex, fi, -1);
        ;

        case NextSibling:
//            return nextSibling(nodeIndex, fi, 1);
        ;
    }

//    Q_ASSERT_X(false, Q_FUNC_INFO, "Don't ever get here!");
    return QXmlNodeModelIndex();
}

QVector<QXmlNodeModelIndex> XmlModel::attributes(const QXmlNodeModelIndex &element) const
{
    QVector<QXmlNodeModelIndex> result;

//    /* Both elements has this attribute. */
//    const QFileInfo &forElement = toFileInfo(element);
//    result.append(toNodeIndex(forElement, AttributeFilePath));
//    result.append(toNodeIndex(forElement, AttributeFileName));

//    if (Type(element.additionalData() == File)) {
//        result.append(toNodeIndex(forElement, AttributeSize));
//        result.append(toNodeIndex(forElement, AttributeSuffix));
//        //result.append(toNodeIndex(forElement, AttributeMIMEType));
//    }
//    else {
//        Q_ASSERT(element.additionalData() == Directory);
//    }

    return result;
}
