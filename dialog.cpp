#include "dialog.h"
#include "ui_dialog.h"

#include <QRegExp>
#include <QFile>
#include <QDebug>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include <QAbstractMessageHandler>

class MessageHandler : public QAbstractMessageHandler
{
public:
    MessageHandler()
        : QAbstractMessageHandler(0)
    {
    }

    QString statusMessage() const
    {
        return m_description;
    }

    int line() const
    {
        return m_sourceLocation.line();
    }

    int column() const
    {
        return m_sourceLocation.column();
    }

protected:
    virtual void handleMessage(QtMsgType type, const QString &description,
                               const QUrl &identifier, const QSourceLocation &sourceLocation)
    {
        Q_UNUSED(type);
        Q_UNUSED(identifier);

        m_messageType = type;
        m_description = description;
        m_sourceLocation = sourceLocation;
    }

private:
    QtMsgType m_messageType;
    QString m_description;
    QSourceLocation m_sourceLocation;
};

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    //regex setup
    ui->regex_edit->setText("^[A-Z]+[a-z]+[A-Z]*[a-z]*\s*,.*[A-Z]{1}[0-9]{1}\s*;{1}.*");


    //xml setup
    QFile xsd("://resource/brief.xsd");
    if (xsd.open(QFile::ReadOnly | QFile::Text))
    {
        ui->xml_xsd->setPlainText(xsd.readAll());
        xsd.close();
    } else {
        qDebug()  << "ERROR: ://resource/brief.xsd failed to open - " << xsd.errorString();
    }
    QFile xml("://resource/brief.xml");
    if (xml.open(QFile::ReadOnly | QFile::Text))
    {
        ui->xml_xml->setPlainText(xml.readAll());
        xml.close();
    } else {
        qDebug()  << "ERROR: ://resource/brief.xml failed to open - " << xml.errorString();
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_regex_edit_textChanged(const QString &regex_edit_text)
{
    QRegExp regex(regex_edit_text);//ui->regex_edit->text());
    regex.setPatternSyntax(QRegExp::RegExp2);
    QString text(ui->regex_text->toPlainText());

    int indx = text.indexOf(regex);
    if (indx > 0){
        QString found = text.at(indx);
        QString bold = QString("<strong>%1</strong>").arg(found);
        text.replace(indx,1,bold);
        text.replace("\n","<br/>");
        ui->regex_text->document()->setHtml(text);
    }

    QString results = QString("%1").arg(regex.capturedTexts().join("\n"));
    ui->regex_results->setText(results);
    QString fb = QString("%1 : %2 : %4 ").arg(indx).arg(regex.captureCount()).arg(regex.errorString());
    ui->regex_feedback->setText(fb);
}


void Dialog::on_xml_validate_clicked()
{
    const QByteArray schemaData = ui->xml_xsd->toPlainText().toUtf8();
    const QByteArray instanceData = ui->xml_xml->toPlainText().toUtf8();

    MessageHandler messageHandler;

    QXmlSchema schema;
    schema.setMessageHandler(&messageHandler);

    schema.load(schemaData);

    bool errorOccurred = false;
    if (!schema.isValid()) {
        errorOccurred = true;
        ui->xml_status->setTextBackgroundColor(Qt::red);
    } else {
        QXmlSchemaValidator validator(schema);
        if (!validator.validate(instanceData))
            errorOccurred = true;
        ui->xml_status->setTextBackgroundColor(Qt::white);
    }

    if (errorOccurred) {
        ui->xml_status->setText(messageHandler.statusMessage());
//        xmlMoveCursor(messageHandler.line(), messageHandler.column());
    } else {
        ui->xml_status->setText(tr("validation successful"));
    }

    const QString styleSheet = QString("QTextBrowser {background: %1; padding: 3px}")
            .arg(errorOccurred ? QColor(Qt::red).lighter(160).name() :
                                 QColor(Qt::green).lighter(160).name());
    ui->xml_status->setStyleSheet(styleSheet);
}

void Dialog::xmlMoveCursor(int line, int column)
{
    ui->xml_xml->moveCursor(QTextCursor::Start);
    for (int i = 1; i < line; ++i)
        ui->xml_xml->moveCursor(QTextCursor::Down);

    for (int i = 1; i < column; ++i)
        ui->xml_xml->moveCursor(QTextCursor::Right);

    QList<QTextEdit::ExtraSelection> extraSelections;
    QTextEdit::ExtraSelection selection;

    const QColor lineColor = QColor(Qt::red).lighter(160);
    selection.format.setBackground(lineColor);
    selection.format.setProperty(QTextFormat::FullWidthSelection, true);
    selection.cursor = ui->xml_xml->textCursor();
    selection.cursor.clearSelection();
    extraSelections.append(selection);

    ui->xml_xml->setExtraSelections(extraSelections);

    ui->xml_xml->setFocus();
}

void Dialog::on_xml_xsd_textChanged()
{
    on_xml_validate_clicked();
}

void Dialog::on_xml_xml_textChanged()
{
    on_xml_validate_clicked();
}
