/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef XMLMODEL_H
#define XMLMODEL_H

#include <QSimpleXmlNodeModel>
#include <QVector>

class XmlModel : public QSimpleXmlNodeModel
{
public:
    XmlModel(QObject *root_object, const QXmlNamePool &namePool);

signals:

public slots:


    // QAbstractXmlNodeModel interface
public:
    QUrl documentUri(const QXmlNodeModelIndex &ni) const;
    QXmlNodeModelIndex::NodeKind kind(const QXmlNodeModelIndex &ni) const;
    QXmlNodeModelIndex::DocumentOrder compareOrder(const QXmlNodeModelIndex &ni1, const QXmlNodeModelIndex &ni2) const;
    QXmlNodeModelIndex root(const QXmlNodeModelIndex &n) const;
    QXmlName name(const QXmlNodeModelIndex &ni) const;
    QVariant typedValue(const QXmlNodeModelIndex &n) const;

protected:
    QXmlNodeModelIndex nextFromSimpleAxis(SimpleAxis axis, const QXmlNodeModelIndex &origin) const;
    QVector<QXmlNodeModelIndex> attributes(const QXmlNodeModelIndex &element) const;

    enum Type {
        Object,
        Field//,
       // AttributeFileName,
    };

    QVector<QXmlName>           m_names;
    QObject *_root_object;

};

#endif // XMLMODEL_H
